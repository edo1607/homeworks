import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')
    text = file.readline()
    text = text.replace("\n", "")
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    if not password.isupper():
        print("Password shall contain only uppercase letters")
        exit(-1)
    i = 0
    encryptedText = ""
    for j in range(0, len(text)):
        n = ord(text[j])+ord(password[i])-ord('A')+1
        encryptedText += chr(n)
        i += 1
        if i == len(password):
            i = 0
    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(encryptedText, password):
    i = 0
    decryptedText = ""
    for j in range(0, len(encryptedText)):
        n = ord(encryptedText[j])-(ord(password[i])-ord('A')+1)
        decryptedText += chr(n)
        i += 1
        if i == len(password):
            i = 0
    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall be passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)
    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)