#include "shape.h"
#include <math.h>

namespace ShapeLibrary {
  Point::Point()
  {
      X = 0;
      Y = 0;
  }
  Point::Point(const double& x, const double& y)
  {
      X = x;
      Y = y;
  }
  Point::Point(const Point& point)
  {
      *this = point;
  }
  double Point::ComputeNorm2(const Point &point) const
  {
      return sqrt(pow(X-point.X,2)+pow(Y-point.Y,2));
  }
  Point Point::operator+(const Point& point) const
  {
      Point p;
      p.X = X + point.X;
      p.Y = Y + point.Y;
      return p;
  }
  Point Point::operator-(const Point& point) const
  {
      Point p;
      p.X = X - point.X;
      p.Y = Y - point.Y;
      return p;
  }
  Point& Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;
  }
  Point& Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }
  ostream& operator<<(ostream& stream, const Point& point)
  {
      stream << "Point: x = " << point.X << " y = " << point.Y << endl;
      return stream;
  }
  Ellipse::Ellipse()
  {
      _center = Point();
      _a = 0;
      _b = 0;
  }
  Ellipse::Ellipse(const Point& center, const double& a, const double& b)
  {
      _center = center;
      _a = a;
      _b = b;
  }
  void Ellipse::SetSemiAxisA(const double &a)
  {
      _a = a;
  }
  void Ellipse::SetSemiAxisB(const double &b)
  {
      _b = b;
  }
  double Ellipse::Perimeter() const
  {
      return M_PI*sqrt(2*(_a*_a+_b*_b));
  }
  Circle::Circle() : Ellipse()
  {

  }
  Circle::Circle(const Point& center,const double& radius) : Ellipse(center, radius, radius)
  {

  }
  Triangle::Triangle()
  {
      points.reserve(3);
  }
  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  {
      points.reserve(3);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
  }
  void Triangle::AddVertex(const Point &point)
  {
      points.push_back(point);
  }
  double Triangle::Perimeter() const
  {
      double perimeter = 0;
      for (unsigned int i = 0; i < 3; i++)
          perimeter += points[i].ComputeNorm2(points[(i+1)%3]);
      return perimeter;
  }
  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge)
  {
      points.reserve(3);
      points[0] = p1;
      points[1] = Point(p1.X+edge, p1.Y);
      points[2] = p1 + Point(edge/2, edge*sqrt(3)/2);
  }
  Quadrilateral::Quadrilateral()
  {
      points.reserve(4);
  }
  Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
      points.reserve(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }
  void Quadrilateral::AddVertex(const Point &p)
  {
      points.push_back(p);
  }
  double Quadrilateral::Perimeter() const
  {
      double perimeter = 0;
      for (unsigned int i = 0; i < 4; i++)
          perimeter += points[i].ComputeNorm2(points[(i+1)%4]);
      return perimeter;
  }
  Rectangle::Rectangle() : Quadrilateral()
  {

  }
  Rectangle::Rectangle(const Point& p1, const double& base, const double& height)
  {
      points.reserve(4);
      points[0] = p1;
      points[1] = Point(p1.X+base, p1.Y);
      points[2] = Point(points[1].X, p1.Y+height);
      points[3] = Point(points[0].X, points[2].Y);
  }
  Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
      points.reserve(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }
  Square::Square() : Rectangle()
  {

  }
  Square::Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
      points.reserve(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }
  Square::Square(const Point& p1, const double& edge) : Rectangle(p1, edge, edge)
  {

  }
}
