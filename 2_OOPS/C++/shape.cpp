#include "shape.h"
#include <math.h>

namespace ShapeLibrary {
Point::Point()
    {
        _x = 0;
        _y = 0;
    }
Point::Point(const double& x, const double& y)
    {
        _x = x;
        _y = y;
    }
Ellipse::Ellipse(const Point& center, const int& a, const int& b)
    {
        _center = center;
        _a = a;
        _b = b;
    }
double Ellipse::Area() const
    {
         return M_PI*_a*_b;
    }
Circle::Circle(const Point& center, const int& radius) : Ellipse(center, radius, radius)
    {

    }
double Circle::Area() const
    {
        return Ellipse::Area();
    }
Triangle::Triangle()
    {
        _p1 = Point();
        _p2 = Point();
        _p3 = Point();
    }
Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
    {
        _p1 = p1;
        _p2 = p2;
        _p3 = p3;
    }
double Triangle::Area() const
    {
        double l12 = sqrt(pow(_p1._x-_p2._x, 2)+pow(_p1._y-_p2._y, 2));
        double l23 = sqrt(pow(_p3._x-_p2._x, 2)+pow(_p3._y-_p2._y, 2));
        double l31 = sqrt(pow(_p3._x-_p1._x, 2)+pow(_p3._y-_p1._y, 2));
        double p = (l12+l23+l31)/2;
        // Erone's formula
        return sqrt(p*(p-l12)*(p-l23)*(p-l31));
    }
TriangleEquilateral::TriangleEquilateral(const Point& p1, const int& edge)
    {
        _p1 = p1;
        _p2 = Point(_p1._x+edge, _p1._y);
        _p3 = Point(_p2._x/2, _p2._y+edge*sqrt(3)/2);
    }
double TriangleEquilateral::Area() const
    {
       return (pow(_p1._x-_p2._x, 2)-pow(_p1._y-_p2._y, 2))*sqrt(3)/4;
    }
Quadrilateral::Quadrilateral()
    {
        _p1 = Point();
        _p2 = Point();
        _p3 = Point();
        _p4 = Point();
    }
Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
    {
        _p1 = p1;
        _p2 = p2;
        _p3 = p3;
        _p4 = p4;
    }
double Quadrilateral::Area() const
    {
        // shoelace formula
        return 0.5*abs(_p1._x*_p2._y+_p2._x*_p3._y+_p3._x*_p4._y+_p4._x*_p1._y-_p2._x*_p1._y-_p3._x*_p2._y-_p4._x*_p3._y-_p1._x*_p4._y);
    }
Parallelogram::Parallelogram() : Quadrilateral()
    {

    }
Parallelogram::Parallelogram(const Point& p1, const Point& p2, const Point& p4)
    {
        _p1 = p1;
        _p2 = p2;
        _p4 = p4;
        _p3 = Point(_p2._x+_p4._x-_p1._x, _p2._y+_p4._y-_p1._y);
    }
double Parallelogram::Area() const
    {
    return Quadrilateral::Area();
    }
Rectangle::Rectangle(const Point& p1, const int& base, const int& height)
    {
        _p1 = p1;
        _p2 = Point(_p1._x+base, _p1._y);
        _p3 = Point(_p1._x, _p1._y+height);
        _p4 = Point(_p2._x, _p3._y);
    }
double Rectangle::Area() const
    {
      return (_p2._x-_p1._x)*(_p3._y-_p2._y);
    }
Square::Square(const Point& p1, const int& edge) : Rectangle(p1, edge, edge)
    {

    }
double Square::Area() const
    {
        return Rectangle::Area();
    }
}
