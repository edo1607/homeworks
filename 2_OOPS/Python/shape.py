import math
class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self):
        return self._a * self._b * math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        Ellipse.__init__(self, center, radius, radius)

    def area(self):
        return Ellipse.area(self)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def area(self):
        """Eulero's formula"""
        l12 = math.sqrt(math.pow(self._p1.x-self._p2.x, 2)+math.pow(self._p1.y-self._p2.y, 2))
        l23 = math.sqrt(math.pow(self._p3.x-self._p2.x, 2)+math.pow(self._p3.y-self._p2.y, 2))
        l31 = math.sqrt(math.pow(self._p1.x-self._p3.x, 2)+math.pow(self._p1.y-self._p3.y, 2))
        p = (l12+l23+l31)/2
        return math.sqrt(p*(p-l12)*(p-l23)*(p-l31))


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        p2 = Point(p1.x+edge, p1.y)
        p3 = Point(p1.x+edge/2, p1.y+edge*math.sqrt(3)/2)
        Triangle.__init__(self, p1, p2, p3)

    def area(self):
        return (self._p2.x-self._p1.x)*(self._p2.x-self._p1.x)*math.sqrt(3)/4


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self):
        """Shoelace formula"""
        return 0.5*abs(self._p1.x*self._p2.y+self._p2.x*self._p3.y+self._p3.x*self._p4.y+self._p4.x*self._p1.y-self._p2.x*self._p1.y-self._p3.x*self._p2.y-self._p4.x*self._p3.y-self._p1.x*self._p4.y)


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        p3 = Point(p2.x+p4.x-p1.x, p2.y+p4.y-p1.y)
        Quadrilateral.__init__(self, p1, p2, p3, p4)

    def area(self):
        return Quadrilateral.area(self)


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        p2 = Point(p1.x+base, p1.y)
        p3 = Point(p1.x, p1.y+height)
        p4 = Point(p2.x, p3.y)
        Quadrilateral.__init__(self, p1, p2, p3, p4)

    def area(self):
        return (self._p2.x-self._p1.x)*(self._p3.y-self._p2.y)


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        Rectangle.__init__(self, p1, edge, edge)

    def area(self):
        return Rectangle.area(self)
