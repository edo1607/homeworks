#include "Pizzeria.h"

namespace PizzeriaLibrary {
void Pizza::AddIngredient(const Ingredient& ingredient)
    {
        PizzaIngredients.push_back(ingredient);
    }
int Pizza::NumIngredients() const
    {
        return PizzaIngredients.size();
    }
int Pizza::ComputePrice() const
    {
        int price = 0;
        for (list<Ingredient>::const_iterator it = PizzaIngredients.begin(); it != PizzaIngredients.end(); ++it)
            price += it->Price;
        return price;
    }
void Order::InitializeOrder(int numPizzas)
    {
        if (numPizzas == 0)
            throw runtime_error("Empty order");
        pizzas.resize(numPizzas);
        numRemainingPizzas = numPizzas;
    }
void Order::AddPizza(const Pizza &pizza)
    {
        pizzas[NumPizzas() - numRemainingPizzas] = pizza;
        numRemainingPizzas--;
    }
const Pizza& Order::GetPizza(const int& position) const
    {
        if (position <= NumPizzas() && position > 0)
        return pizzas[position-1];
        else throw runtime_error("Position passed is wrong");
    }
int Order::NumPizzas() const
    {
        return pizzas.size();
    }
int Order::ComputeTotal() const
    {
        unsigned int total = 0;
        for (int i = 0; i < NumPizzas(); i++)
            total += pizzas[i].ComputePrice();
        return total;
    }
void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
    {
        list<Ingredient>::const_iterator it;
        for (it = kitchen.begin(); it != kitchen.end(); ++it)
            if (it->Name == name)
                break;
        if (it != kitchen.end())
            throw runtime_error("Ingredient already inserted");
        Ingredient ingredient;
        ingredient.Name = name;
        ingredient.Description = description;
        ingredient.Price = price;
        kitchen.push_back(ingredient);
    }
const Ingredient& Pizzeria::FindIngredient(const string &name) const
    {
        list<Ingredient>::const_iterator it;
        for (it = kitchen.begin(); it != kitchen.end(); ++it)
            if (it->Name == name)
                break;
        if (it == kitchen.end())
            throw runtime_error("Ingredient not found");
        return *it;
    }
void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
    {
        list<Pizza>::const_iterator it;
        for (it = menu.begin(); it != menu.end(); ++it)
            if (it->Name == name)
                break;
        if (it != menu.end())
            throw runtime_error("Pizza already inserted");
        Pizza pizza;
        pizza.Name = name;
        for (unsigned int i = 0; i < ingredients.size(); i++)
            pizza.PizzaIngredients.push_back(FindIngredient(ingredients[i]));
        menu.push_back(pizza);
    }
const Pizza& Pizzeria::FindPizza(const string &name) const
    {
        list<Pizza>::const_iterator it;
        for (it = menu.begin(); it != menu.end(); ++it)
            if (it->Name == name)
                break;
        if (it == menu.end())
            throw runtime_error("Pizza not found");
        return *it;
    }
int Pizzeria::CreateOrder(const vector<string> &pizzas)
    {
        if (pizzas.size() == 0)
            throw runtime_error("Empty order");
        Order order;
        order.InitializeOrder(pizzas.size());
        for (unsigned int i = 0; i < pizzas.size(); i++)
            order.AddPizza(FindPizza(pizzas[i]));
        clients.push_back(order);
        return clients.size() + 999;
    }
const Order& Pizzeria::FindOrder(const int &numOrder)
    {
        list<Order>::const_iterator it;
        for (it = clients.begin(); it != clients.end(); ++it)
            if (distance<list<Order>::const_iterator>(clients.begin(), it) == numOrder - 1000)
                break;
        if (it == clients.end())
            throw runtime_error("Order not found");
        return *it;
    }
string Pizzeria::GetReceipt(const int &numOrder) const
    {
        list<Order>::const_iterator it;
        for (it = clients.begin(); it != clients.end(); ++it)
            if (distance<list<Order>::const_iterator>(clients.begin(), it) == numOrder - 1000)
                break;
        if (it == clients.end())
            throw runtime_error("Order not found");
        string receipt;
        int bill = 0;
        for (unsigned int i = 0; i < it->pizzas.size(); i++)
        {
            receipt.append("- " + it->pizzas[i].Name + ", " + to_string(it->pizzas[i].ComputePrice()) + " euro" + '\n');
            bill += it->pizzas[i].ComputePrice();
        }
        receipt.append("  TOTAL: " + to_string(bill) + " euro" + '\n');
        return receipt;
    }
string Pizzeria::ListIngredients() const
    {
        string kitchenAZ;
        list<Ingredient> k = kitchen;
        k.sort([](const Ingredient& i1, const Ingredient& i2) {return i1.Name.compare(i2.Name);});
        for (list<Ingredient>::const_iterator it = k.begin(); it != k.end(); ++it)
            kitchenAZ.append(it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + '\n');
        return kitchenAZ;
    }
string Pizzeria::Menu() const
    {
        string List;
        for (list<Pizza>::const_iterator it = menu.begin(); it != menu.end(); ++it)
            List += it->Name + " (" + to_string(it->NumIngredients()) + " ingredients): " + to_string(it->ComputePrice()) + " euro" + '\n';
        return List;
    }
}
