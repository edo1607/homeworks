class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.pizzaIngredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.pizzaIngredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.pizzaIngredients)

    def computePrice(self) -> int:
        price = 0
        for ingredient in self.pizzaIngredients:
            price += ingredient.Price
        return price


class Order:
    def __init__(self):
        self.pizzas = []
        self.NumRemainingPizzas = 0

    def getPizza(self, position: int) -> Pizza:
        if 0 < position <= Order.numPizzas(self):
            return self.pizzas[position - 1]
        else:
            raise ValueError("Position passed is wrong")

    def initializeOrder(self, numPizzas: int):
        if numPizzas == 0:
            raise ValueError("Empty order")
        pizza = Pizza("")
        self.pizzas = [pizza] * numPizzas
        self.NumRemainingPizzas = numPizzas

    def addPizza(self, pizza: Pizza):
        self.pizzas[Order.numPizzas(self) - self.NumRemainingPizzas] = pizza
        self.NumRemainingPizzas -= 1

    def numPizzas(self) -> int:
        return len(self.pizzas)

    def computeTotal(self) -> int:
        total = 0
        for i in range(0, Order.numPizzas(self)):
            total += self.pizzas[i].computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.kitchen = []
        self.Menu = []
        self.clients = []

    def addIngredient(self, name: str, description: str, price: int):
        found = False
        for element in self.kitchen:
            if element.Name == name:
                found = True
                break
        if found:
            raise ValueError("Ingredient already inserted")
        ingredient = Ingredient(name, price, description)
        self.kitchen.append(ingredient)

    def findIngredient(self, name: str) -> Ingredient:
        found = False
        ingredient = Ingredient("", 0, "")
        for ingredient in self.kitchen:
            if ingredient.Name == name:
                found = True
                break
        if not found:
            raise ValueError("Ingredient not found")
        return ingredient

    def addPizza(self, name: str, ingredients: []):
        found = False
        for element in self.Menu:
            if element.Name == name:
                found = True
                break
        if found:
            raise ValueError("Pizza already inserted")
        pizza = Pizza(name)
        for ingredient in ingredients:
            pizza.pizzaIngredients.append(Pizzeria.findIngredient(self, ingredient))
        self.Menu.append(pizza)

    def findPizza(self, name: str) -> Pizza:
        found = False
        pizza = Pizza("")
        for pizza in self.Menu:
            if pizza.Name == name:
                found = True
                break
        if not found:
            raise ValueError("Pizza not found")
        return pizza

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise ValueError("Empty order")
        order = Order()
        order.initializeOrder(len(pizzas))
        for i in range(0, len(pizzas)):
            order.addPizza(Pizzeria.findPizza(self, pizzas[i]))
        self.clients.append(order)
        return len(self.clients) + 999

    def findOrder(self, numOrder: int) -> Order:
        found = False
        order = Order()
        for order in self.clients:
            if self.clients.index(order) == numOrder - 1000:
                found = True
                break
        if not found:
            raise ValueError("Order not found")
        return order

    def getReceipt(self, numOrder: int) -> str:
        found = False
        order = Order()
        for order in self.clients:
            if self.clients.index(order) == numOrder - 1000:
                found = True
                break
        if not found:
            raise ValueError("Order not found")
        receipt = ""
        bill = 0
        for i in range(0, len(order.pizzas)):
            price = str(order.pizzas[i].computePrice())
            receipt += "- " + order.pizzas[i].Name + ", " + price + " euro" + '\n'
            bill += order.pizzas[i].computePrice()
        receipt += "  TOTAL: " + str(bill) + " euro" + '\n'
        return receipt

    def listIngredients(self) -> str:
        kitchenAZ = ""
        k = self.kitchen
        k.sort(key=lambda x: x.Name)
        for element in k:
            price = str(element.Price)
            kitchenAZ += element.Name + " - '" + element.Description + "': " + price + " euro" + '\n'
        return kitchenAZ

    def menu(self) -> str:
        List = ""
        for pizza in self.Menu:
            price = str(pizza.computePrice())
            List += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + price + " euro" + '\n'
        return List
