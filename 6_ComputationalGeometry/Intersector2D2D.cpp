#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceIntersection = 1E-7;
    toleranceParallelism = 1E-7;
}
Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    Vector3d NormalFirstSecond = matrixNormalVector.row(0).cross(matrixNormalVector.row(1));
    if (NormalFirstSecond.squaredNorm() > toleranceParallelism * toleranceParallelism * matrixNormalVector.row(0).squaredNorm() * matrixNormalVector.row(1).squaredNorm())
    {
        matrixNormalVector.row(2) = NormalFirstSecond;
        rightHandSide(2) = 0;
        pointLine = matrixNormalVector.inverse() * rightHandSide;
        tangentLine = NormalFirstSecond;
        intersectionType = LineIntersection;
        return true;
    }
    else
    {
        if (fabs(rightHandSide(0) - rightHandSide(1)) <= toleranceIntersection * fabs(rightHandSide(0)))
            intersectionType = Coplanar;
        return false;
    }
}
