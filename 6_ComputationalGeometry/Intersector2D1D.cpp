#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1E-7;
    toleranceParallelism = 1E-7;
}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
}
// ***************************************************************************
Vector3d Intersector2D1D::IntersectionPoint()
{
        return *lineOriginPointer + intersectionParametricCoordinate*(*lineTangentPointer);
}
bool Intersector2D1D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    int NormalTangent = 0, NormalOrigin = 0;
    NormalTangent = planeNormalPointer->dot(*lineTangentPointer);
    NormalOrigin = planeNormalPointer->dot(*lineOriginPointer);
    if (NormalTangent * NormalTangent > toleranceParallelism * toleranceParallelism * planeNormalPointer->squaredNorm() * lineTangentPointer->squaredNorm())
    {
        intersectionParametricCoordinate = (*planeTranslationPointer - NormalOrigin) / NormalTangent;
        intersectionType = PointIntersection;
        return true;
    }
    else
    {
        if (fabs(NormalOrigin - *planeTranslationPointer) <= fabs(toleranceIntersection * NormalOrigin))
            intersectionType = Coplanar;
        return false;
    }
}
